#ifndef __BIAWEBNAVIGATIONBIT__
#define __BIAWEBNAVIGATIONBIT__

#include <list>
#include <fstream>
#include <fmt/format.h>
#include "biawebutil.hpp"
#include "biawebsidebar.hpp"
#include "biawebtemplate.hpp"

namespace biaweb {
    // class to represent a navigation bit like this in html
    // > Parent > Child > Subchild
    // etc.
    class NavigationBit {
      protected:
      std::list<GenericLinkItem> items;
      public:
        // add a generic link item - this should add to beginning of the list
        void add_link_item (GenericLinkItem item) {
            items.insert (items.cbegin(), item);
        }

        // render using the given template directory
        std::string to_html (Template *t) ;
    };

    // render using the given template 
    std::string NavigationBit::to_html (Template *t) {
        std::string templ_str = t->get_navigationbit_tpl ();

        std::string output_html = "";
        std::string items_str = "";
        for (GenericLinkItem item : this->items)
            items_str += item.to_html (t);

        if (this->items.size () > 0)
            output_html = fmt::format (templ_str, fmt::arg ("items", items_str));

        return output_html;
    }
}

#endif