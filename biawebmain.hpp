#ifndef __BIAWEBMAIN__
#define __BIAWEBMAIN__
#include <unistd.h>
#include <chrono>
#include "biawebdocumenttree.hpp"
#include "biawebstrings.hpp"
namespace biaweb { 
    // Class to parse the command line options and run DocumentTree
    class Main {
      private:
        bool quiet_mode;
        bool display_usage;
        std::string app_name; 
        std::string target_dir;
        std::string template_dir;
        std::string source_dir;

      public:
        // main constructor - build options from command line
        Main (int argc, char **argv) ;

        // run the program with the options built from constructor 
        // and return the result
        int run ();
    };

    // Execute the program and return the result
    int Main::run () {
        // if usage of the program is to be displayed then display it and quit
        if (this->display_usage == true ) {
            std::cerr << PROGRAM_USAGE;
            std::cerr << this->app_name << " " << PROGRAM_ARGS << std::endl;
            std::cerr << PROGRAM_EXPLANATION << std::endl;
            return 1;
        }
        // else run the program
        else {
            // timing for the program
            std::chrono::time_point<std::chrono::steady_clock> t1 
                            (std::chrono::steady_clock::now());

            // create a blank document tree and build the tree from source dir
            std::unique_ptr<DocumentTree> doctree (new DocumentTree(""));
            doctree.get()->document_tree_builder (this->source_dir);
            // if not quiet mode, display the tree visualization
            if (! this->quiet_mode) {
                std::cout << VISUALIZE_TREE << std::endl;
                doctree.get()->visualize_tree (); 
            }
            // generate the template
            Template t (this->template_dir);
            doctree.get()->create_tree_html (&t, this->target_dir);

            if (! this->quiet_mode)
                std::cout << OUTPUT_IN << " " << this->target_dir << std::endl;
            
            // end time
            std::chrono::time_point<std::chrono::steady_clock> t2 
                            (std::chrono::steady_clock::now());
            std::chrono::duration<double> dur (t2 - t1);
            // display the execution time
            if (! this->quiet_mode)
                std::cout << GENERATED_IN << dur.count() << std::endl;
        }
        return 0;
    }

    // build the options from command line using getopt
    Main::Main (int argc, char **argv) {
        this->display_usage = true;
        this->quiet_mode = false;
        this->app_name = argv[0];
        this->source_dir = "";
        this->target_dir = "";
        this->template_dir = "";
        int c;

        while ((c = getopt (argc, argv, "i:t:o:q")) != -1) {
            switch (c) {
                case 'i' : this->source_dir = optarg;
                           break;
                case 't' : this->template_dir = optarg;
                           break;
                case 'o' : this->target_dir = optarg;
                           break;
                case 'q' : this->quiet_mode = true;
            }
        }
        // if the user has not given the required parameters then set to display usage
        // mode
        if (this->source_dir.empty () || this->target_dir.empty () 
            || this->template_dir.empty ())
            this->display_usage = true;
        else 
            this->display_usage = false;
    }
}

#endif
