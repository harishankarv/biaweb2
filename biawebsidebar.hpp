#ifndef __BIAWEBSIDEBAR__
#define __BIAWEBSIDEBAR__
#include <string>
#include <list>
#include <iostream>
#include <fstream>
#include <fmt/format.h>
#include "biawebutil.hpp"
#include "biawebtemplate.hpp"

// classes to describe the a list of (link) items and sidebar containers which form part of 
// main document
namespace biaweb {
    // class to represent a generic link item which can contain a text and link or only
    // text
    class GenericLinkItem {
      protected:
        // sidebar text and url
        std::string itemtext;
        std::string itemurl;
      public:
        std::string get_item_text () {
            return this->itemtext;
        }
        void set_item_text (std::string text) {
            this->itemtext = escape_html (text);
        }
        std::string get_item_url () {
            return this->itemurl;
        }
        void set_item_url (std::string url) {
            this->itemurl = escape_html (url);
        }
        // output to HTML using a template specified
        std::string to_html (Template *t);
    
        GenericLinkItem (std::string text = "", std::string url = "") {
            this->itemtext = escape_html (text);
            this->itemurl = escape_html (url);
        }
    };

    std::string GenericLinkItem::to_html (Template *t) {
        std::string html = "";
        // if url is not empty it is a link item so load the sidebar link template
        if (! this->itemurl.empty ()) {
            if (!this->itemtext.empty ())
            {
                std::string tpl_linkitem_str = t->get_genericlinklistitem_tpl ();
                html = fmt::format (tpl_linkitem_str, 
                                        fmt::arg ("itemurl", this->itemurl),
                                        fmt::arg ("itemtext", this->itemtext));
            }
        }
        // Non link item. Load the normal sidebar item template.
        else if (! this->itemtext.empty ())
        {
            std::string tpl_item_str = t->get_genericlistitem_tpl ();
            html = fmt::format (tpl_item_str, fmt::arg ("itemtext", this->itemtext) );
        } 
        return html;
    }
    
    // Class to represent a sidebar, which contains one heading and a list of items 
    // either links or non-link items.
    class SideBar {
      protected:
        std::string sidebar_title;
        std::list <GenericLinkItem> items;
        
      public:
        void set_title (std::string title) {
            this->sidebar_title = title;
        }

        void add_sidebar_item (GenericLinkItem item) {
            this->items.insert (this->items.cend(), item);
        }

        // render the sidebar using the template specified
        std::string to_html (Template *t) ;
    };

    // render the sidebar to HTML representation from the template.
    std::string SideBar::to_html (Template *t) {
        std::string sidetpl_str = t->get_sidebar_tpl ();

        std::string listitems;
        // first get the sidebar items and render them to HTML
        for (GenericLinkItem item : this->items) {
            listitems += item.to_html (t);
        }
        
        std::string html = "";
        // if there are items, sidebar should be rendered otherwise not needed
        if (items.size () > 0)
            html = fmt::format (sidetpl_str, 
                                        fmt::arg ("title", this->sidebar_title),
                                        fmt::arg ("items", listitems));

        return html; 
    }
}

#endif
