#ifndef __BIAWEB__
#define __BIAWEB__
#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <memory>
#include <iomanip>
#include <ctime>
#include <fmt/format.h>
#include "biawebutil.hpp"
#include "biawebsidebar.hpp"
#include "biawebstrings.hpp"
#include "biawebnavigationbit.hpp"
#include "biawebtemplate.hpp"

// class to represent a biaweb document which can have a file name, title, description, 
// keywords, content and sidebar items
namespace biaweb {
    class Document
    {
      protected:
        std::string filename; 
        std::string title;
        std::string meta_desc;
        std::string meta_keywords;
        std::string content;
        std::list<SideBar> sidebars;
        NavigationBit navbit; 
        std::time_t cdate;
        std::time_t mdate;
        bool is_index;

      public:
        Document (std::string title = "", std::string meta_desc = "", 
                    std::string meta_keywords = "", std::string content = "", 
                        bool is_index = false, std::time_t cdate= std::time(nullptr),
                            std::time_t mdate = std::time(nullptr))
        {
            this->title = escape_html (title);
            this->meta_desc = escape_html (meta_desc);
            this->meta_keywords = escape_html (meta_keywords);
            this->content = content;
            this->is_index = is_index;
            if (! is_index)
                this->filename = convert_title (title);
            else 
                this->filename = "index";
            this->cdate = cdate;
            this->mdate = mdate;
        }

        // Constructor to parse the document from a stream instead of manually creating it 
        // File should be of the format
        // first line: title
        // second line: Description
        // third line: Keywords 
        // fourth line: (creation date) YYYY-MM-DD HH:II TZ format
        // fourth line onwards: Markdown contents
        Document (std::istream &file) ;

        // set the navigation bit
        void set_navigation_bit (NavigationBit navbit) {
            this->navbit = navbit;
        }

        // set whether this is the index document
        void set_index (bool index = true) {
            this->is_index = index; 
            index == true ? this->filename = "index" : 
                            this->filename = convert_title (this->title);
        }

        // get whether this is the index document
        bool get_index () {
            return this->is_index;
        }

        // get the file name
        std::string get_filename () {
            return this->filename;
        }

        // set the document modification date
        void set_modified_date (std::time_t modif) {
            this->mdate = modif;
        }

        // get the document modification date
        std::time_t get_modified_date () {
            return this->mdate; 
        }

        // set the document creation date
        void set_creation_date (std::time_t creat) {
            this->cdate = creat;
        }

        // get the document creation date
        std::time_t get_creation_date () {
            return this->cdate;
        }

        // output the document to HTML using the template specified
        void output_to_html (Template *t, std::string path); 

        // set the content portion of document as raw HTML content
        void set_content (std::string content) {
            this->content = content;
        }

        // read the contents of marked marked-up content string "str" into the 
        // contents after converting to HTML.
        void set_markdown_content (std::string str); 

        void set_meta_keywords(std::string meta_keywords) {
            this->meta_keywords = escape_html (meta_keywords);
        }
        
        void set_meta_desc(std::string meta_desc) {
            this->meta_desc = escape_html (meta_desc);
        }
        
        void set_title(std::string title) {
            this->title = title;
            if (this->is_index)
                this->filename = "index";
            else
                this->filename = convert_title (title);
        }

        std::string get_content () {
            return this->content;
        }

        std::string get_meta_keywords() {
            return this->meta_keywords;
        }
        
        std::string get_meta_desc() {
            return this->meta_desc;
        }
        
        std::string get_title() {
            return this->title;
        }
        
        void add_side_bar (SideBar bar) {
            sidebars.insert (sidebars.cend(), bar);
        }
    };

    // Parse and construct a document from a stream instead of individual fields
    Document::Document (std::istream &infile) {
        infile.seekg (0);
        // parse the title, description, keywords, creation time and and contents
        std::string title, description, keywords, creattime, contents;
        // read the title
        std::getline (infile, title);
        if (infile.eof ()) return;
        this->title = escape_html (title);
        this->filename = convert_title (title);
        // read description
        std::getline (infile, description);
        if (infile.eof ()) return;
        this->meta_desc = escape_html (description);
        // read the keywords
        std::getline (infile, keywords);
        if (infile.eof ()) return;
        this->meta_keywords = escape_html (keywords); 
        // read the creation date/time and also set the modification time
        // to creation date/time by default
        std::getline (infile, creattime);
        if (infile.eof ()) return; 
        std::stringstream s (creattime);
        std::tm t;
        s >> std::get_time (&t, DATE_IN_FORMAT);
        if (s.fail ())
            std::cout << WARNING_PARSE_FAILED << this->filename << "\n";
 
        this->cdate = mktime (&t);
        this->mdate = this->cdate;
        // read the rest of contents
        std::string line;
        while (! infile.eof ()) {
            std::getline (infile, line);
            contents.append (line + "\n");
        }
        this->set_markdown_content (contents);
    }

    void Document::set_markdown_content (std::string str) {
        this->content = convert_to_markdown (str);
    }
    
    // output the document using the provided template
    void Document::output_to_html (Template *t, std::string path)
    {
        std::string templstr = t->get_main_tpl ();

        // read the style template file
        std::string stylesheet = t->get_style_tpl ();
        // first render the sidebars
        std::string sidebartext;
        for (SideBar bar : sidebars) {
            sidebartext += bar.to_html (t);
        }

        // render the navigation bit
        std::string navbit_str = this->navbit.to_html (t);

        // time of creation and modification 
        struct std::tm c, m;
        c = *std::localtime (&this->cdate);
        m = *std::localtime (&this->mdate);

        // format the template with the values
        std::string outputhtml = fmt::format (templstr, 
                                    fmt::arg ("title", this->title),
                                    fmt::arg ("keywords", this->meta_keywords), 
                                    fmt::arg ("stylesheet", stylesheet),
                                    fmt::arg ("description", this->meta_desc),
                                    fmt::arg ("cdate", c),
                                    fmt::arg ("mdate", m),
                                    fmt::arg ("navbit", navbit_str),
                                    fmt::arg ("contents", this->content),
                                    fmt::arg ("sidebar", sidebartext)
                                    );

        std::ofstream f (path + "/" + this->filename + ".html");
        f << outputhtml;
        f.close ();
    }
}

#endif
