#ifndef __BIAWEBUTIL__
#define __BIAWEBUTIL__
#include <string>
#include <fstream>

// "discount" markdown library is a C library and hence requires to be wrapped in 
// extern "C"
extern "C" {
    #include <mkdio.h>
}

// utility functions for Biaweb that don't fit into any class and can be used by 
// any class
namespace biaweb {
    // load a string from file
    std::string load_from_file (std::string filename) {
        std::ifstream f (filename);
        std::string r ((std::istreambuf_iterator<char> (f)),
                        (std::istreambuf_iterator<char> ()));
        return r;
    }

    // convert markdown
    std::string convert_to_markdown (std::string str) {
        // discount is a C library and it doesn't work well with C++ streams
        // and there seems no way to get the output of any of these functions
        // into an std::string. 
        // the only option seems to be to write the output of the markdown()
        // function to a temporary working file and then read it back into C++ 
        // with the normal std::ifstream and feed it into the std::string 
        // till a cleaner solution can be found. 
        MMIOT *doc;
        doc = mkd_string (str.c_str(), str.size(), 0);
        char tempfile[20];
        strcpy (tempfile, "/tmp/biawebXXXXXX");
        int fd = mkstemp (tempfile);
        FILE *f = fdopen (fd, "w");
        markdown (doc, f, 0);
        fclose (f);
        std::string tmpl = load_from_file (tempfile);
        mkd_cleanup (doc);
        remove (tempfile);
        return tmpl;
    }

    // convert a document title to a file title - strip out the non-alpha
    // chars and spaces
    std::string convert_title (std::string title)
    {
        std::string output;
        for (char c : title) {
            if (isalnum (c))
                output.append (1, c);
            else if (isspace (c))
                output.append (1, '_');
        }
        return output;
    }

    // escape HTML special characters 
    std::string escape_html (std::string source)
    {
    	std::string replace_buf;
        replace_buf.reserve (source.size());
        for (char p : source)
        {
            switch (p)
            {
                case '&' : replace_buf.append ("&amp;"); break;
                case '<' : replace_buf.append ("&lt;"); break;
                case '>' : replace_buf.append ("&gt;"); break;
                case '\"': replace_buf.append ("&quot;"); break;
                case '\'': replace_buf.append ("&apos;"); break;
                default  : replace_buf.append (1, p); 
            }
        }
        return replace_buf;
    }
}
#endif