#ifndef __BIAWEBSTRINGS__
#define __BIAWEBSTRINGS__

namespace biaweb {
    // COMMON APPLICATION STRINGS
    const char* WARNING_PARSE_FAILED = "Warning: date parse failed on " ;
    const char* NO_SUCH_PATH_ERROR = "No such path! Specify an existing file path" ;
    const char* DATE_IN_FORMAT = "%Y-%m-%d %H:%M %Z";
    const char* GENERATED_IN = "Generated in (s): ";
    const char* VISUALIZE_TREE = "Document tree: ";
    const char* PROGRAM_ARGS = " -i <inputdir> -o <outputdir> -t <templatedir> [-q]";
    const char* OUTPUT_IN = "Output in: ";
    const char* PROGRAM_USAGE = "Usage: ";
    const char* PROGRAM_EXPLANATION = "Where \n \
\t-i <inputdir>    - source directory tree with markdown sources\n \
\t-o <outputdir>   - destination in which to create the HTML output\n \
\t-t <templatedir> - directory containing the templates\n \
\t-q               - quiet mode (don't display messages\n \
NOTE: directory argument need not terminate with the separator (/)";

    // MAXIMUM SIZE OF RSS FEED
    const unsigned int MAX_RSS_FEED = 10;

    // NAMED INDEX FOR DOCUMENT STRING BITS (LOADED FROM TEMPLATE FILE stringbits.txt)
    const unsigned int HOME = 0;
    const unsigned int ARTICLES_LIST = 1;
    const unsigned int INDEX = 2;
    const unsigned int NAVIGATION = 3;
    const unsigned int SUB_CAT = 4;
    const unsigned int GO_UP = 5;
    const unsigned int SUBSCRIBE = 6;
    const unsigned int RSS_FEED = 7;

}

#endif
