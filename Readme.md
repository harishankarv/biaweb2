# Readme

## Overview

BiaWeb2 is a static site generator written in C++, that takes a directory tree 
of documents (in Markdown format - more on that later) and outputs a complete website 
with the same tree structure and proper navigation, based on a set of templates. 

BiaWeb2 supports infinite nesting level of source sub-directories, with 
each directory representing a "category". Thus, a website can have (say)
a structure like

    + Home
    + ----- Blog
    + ------+------ Travel
    + ------+------ History
    + ------+------ Personal
    + ------+------ Technology      
    + ----- Creative
    + ------+------ Reviews
    + ------+------ +------- Books
    + ------+-------+--------+-------- Fiction
    + ------+-------+--------+-------- Non-fiction
    + ----- Miscellaneous

Thus the entire structure of the website is represented by the filesystem
hierarchy of directories (which I think is an intuitive way of organizing
categories).

`biaweb2` is a command-line program that performs the entire website 
generation in one step. The usage is:

    biaweb2 -i <inputdir> -o <outputdir> -t <templatedir> [-q]

Where `<inputdir>` is the input directory, `<outputdir>` is the destination
directory and `<templatedir>` is the location of the template files. The
`templates` directory in the __biaweb2__ source package provides a default
template that can be further customized to suit your needs. The main philosophy
of BiaWeb2 is to be as configuration-free as possible and hence most of the 
customization for a specific website is within the templates themselves. The
option `-q` is quiet-mode, to suppress the messages. 

## Getting and compiling BiaWeb2 

BiaWeb2 works on *nix-like operating systems. 

To get BiaWeb2, clone the git repository (from the command line)

    git clone https://gitlab.com/harishankarv/biaweb2.git

To compile you require the following dependencies and a C++ compiler. Check your
distribution package manager for more information

* libmarkdown2-dev or equivalent package 
   ([discount](https://www.pell.portland.or.us/~orc/Code/discount/))
* libfmt-dev or equivalent package ([fmt](https://github.com/fmtlib/fmt))
* scons ([scons](https://scons.org/)) 

Once you have got the dependencies, compile using scons from inside the 
source directory of BiaWeb2 wherein you had cloned the repository

    cd biaweb2/
    scons

If successful, you should get an executable file `biaweb2` in the same directory
If not, check whether you have installed all the requirements.

## Usage

### Getting started - creating the sources

Once you have successfully compiled BiaWeb2, you will want to create your website
sources.

This is probably the longest step, since you need to create the directory structure
of your website (you probably want to plan this properly, though you can of course
easily change the structure by rearranging or moving the folders as needed). You also
need to create the content of your website. 

A typical workflow is described below: 

1. First create a base directory for your entire website project (including sources, 
   templates, static resources like images etc.) Replace `~/path/to/mywebsite` with 
   your own path.

      ````
      mkdir ~/path/to/mywebsite
      cd ~/path/to/mywebsite
      ````

2. Copy the default template from the BiaWeb2 source directory to your website path

      ````
      cp -r /path/to/biaweb2/templates ~/path/to/mywebsite/
      ````
3. Create a source directory that will hold your website contents. Name this directory
   as "Home" or similar (this name will be used as the base name of your website)

      ````
      mkdir ~/path/to/mywebsite/Home
      ````

4. Now you can add all the categories within this folder. Let's say you want a section
   for blogs. Create a directory called _Blog_ within the _Home_ folder

      ````
      mkdir ~/path/to/mywesbite/Home/Blog
      ````

5.  Under each category you can create the regular content as text files, with the following 
    simple format  (title, description, keywords, creation date/time followed by 
    the markdown text. Each of title, description, keywords and creation date/time lives 
    in its own line):

      ````
      Title of the article
      some description of the article
      keywords,keyword 1,keyword2
      2020-05-20 18:00 UTC
      markdown contents of the document
      ...
      ````
    Here, the first line is the title, followed by description on the second line, keywords on
    the third line and date/time of creation of the article (in YYYY-MM-DD HH:II Z format). If
    the format of date/time differs, BiaWeb2 will issue a warning when generating the website
    and you will have a wrong date/time stamp on the article in the generated page. All four
    (title, description, keywords and creation date/time are mandatory).

6.  Using the above format, add as many documents as you want within the created categories.
    There is no special file extension required as BiaWeb2 will read all the files in the 
    source directories. However, you can conventionally use the `.md` extension to allow your
    favourite text editor to recognize the file as markdown and provide syntax highlighting
    and/or completion etc. You can keep adding more categories and articles as you wish. 
    Note that, symbolic links will not get followed by BiaWeb2 and hence keep all the files 
    as regular text files. 

7. You can optionally have a special named `index.md` or `index` file in every folder (case 
   sensitive though the file extension does not matter) to describe the index page 
   generated by BiaWeb2.  The content of this file is pure Markdown and will be 
   shown as a summary on the __index.html__ page generated by BiaWeb2 under each folder 
   (category). Apart from the content of the `index` file, the generated __index.html__ file 
   will also automatically create links to the articles under the category and the links 
   to the  sub-categories. For example, under the _Home_ folder we can create and index.md 
   file with the following content:

      ````
      # Hello and Welcome to my website

      Hello and welcome to my website. Here you can find my writings on various topics. 
      Please explore the various categories more information
      ````
    
    The above will be used as a "summary" to describe the category.

### Setting up the templates

If you copied the _templates_ folder correctly from the BiaWeb2 sources, the _templates_ folder 
within your `~/path/to/mywebsite/~ will have the following files:

* doclistitem.tpl.html
* doclist.tpl.html
* genericlinklistitem.tpl.html
* genericlistitem.tpl.html
* __main.tpl.html__
* navigationbit.tpl.html
* __rssitem.tpl.xml__
* __rss.tpl.xml__
* sidebar.tpl.html
* stringbits.txt
* style.tpl.css

The minimum necessary files to be edited to get your website working are __main.tpl.html__, 
__rssitem.tpl.xml__ and __rss.tpl.xml__ as those files need your website URL in 
the right places to work properly. The __style.tpl.css__ file is also of interest if you
want to modify the default look and feel of your site.

__main.tpl.html__ is the base template and the file looks like this by default (only the key
parts are highlighted for clarity):

    <!DOCTYPE html>
    ...
    <!-- change as appropriate - just don't touch anything within the curly braces!! -->
    <title>My Site - {title}</title>
    ...
    <meta name="keywords" content="site key words {keywords}">
    <meta name="description" content="Site Description - {description}">
    <!-- change base href to your site URL -->
    <base href="http://my.site">
    ...  
    <body>
        <!-- change My Site to your own header -->
        <header><h1>My Site</h1></header>
        ...
        ...

        <footer>
            <!-- change copyright to your own copyright -->
            My copyright
        </footer>
    </body>
    </html>

As suggested don't touch anything within the curly braces in this file or any other
template file in this folder as they represent the template variables. Change the
site URL and title, keywords, description, header and copyright as per your requirements 
and save. Again, __DO NOT__ delete or edit anything within `{` and `}`.

Note that, BiaWeb2 relies on the `<base>` HTML tag to be present to generate the proper
links structure. There are, of course, pros and cons to using this tag, but at present, 
this tag is required to generate a site with BiaWeb2.

__rss.tpl.xml__ and __rssitem.tpl.xml__ are required to generate the RSS feed for
each category (note that RSS feeds are generated only for documents/articles within that
category alone and not any sub-categories. This is a limitation at present).

Again, change the title, description and URL as required:

__rss.tpl.xml__

    <?xml version="1.0" encoding="UTF-8" ?>
    <rss version="2.0">
    <channel>
        <!-- change title description and link as needed-->
        <!-- don't touch anything within the curly braces -->
        <title>My Site - {title}</title>
        <description>Site Description</description>
        <link>http://my.site</link>
        <pubDate>{pubdate:%a, %d %b %Y %H:%M:%S %z}</pubDate>
        {items}
    </channel>
    </rss>

__rssitem.tpl.xml__

    <item>
    ...    
    <!-- change http://my.site to your actual site URL -->
    <!-- don't touch anything within the curly braces-->
    <link>http://my.site/{url}</link>
    <guid>http://my.site/{url}</guid>
    <pubDate>{pubdate:%a, %d %b %Y %H:%M:%S %z}</pubDate>
    </item>

Change the URL part `http://my.site/` to your site's url (dont't remove the 
`{url}` bit from the above)

That's it. Now your templates are set up to generate your website with proper links. 

Though this process looks tedious, it is basically a one-time setup. Once your template is
configured, you almost never need to touch it again (unless you are changing the URL of
your website)

### Generating the output 

Now we can invoke `biaweb2` on your website source. To make invoking `biaweb2` convenient
make a small shell script called `gensite.sh` within `~/path/to/mywebsite` with the 
following contents:

    !/bin/sh 
    /path/to/biaweb2/biaweb2 -i Home -t templates -o Out

and change it to executable (from the command line):

    chmod +x ~/path/to/mywebsite/gensite.sh

Invoke the `gensite.sh` script from within the `~/path/to/mywebsite` directory:

    ./gensite.sh

You should get output similar to this

    Document tree: 
    0  Home
    1  +--Blog
    Output in:  Out
    Generated in (s): 0.0848904

Assuming you have only one folder _Blog_ inside the _Home_ folder. 

__Note__ that if you provide a trailing `/` to your input folder parameter `-i` 
then the "base" node of your tree  will not have any name - it will simply be 
blank. Your output would look like this:

    Document tree: 
    0
    1  +--Blog
    Output in:  Out
    Generated in (s): 0.0848904

Use this "feature" as you wish.

That's it. You can now transfer the contents of the _Out_ folder to your web host
through FTP or SFTP.

### Updating your website

Simply make any changes to the source folder (by creating content files and folders) and 
invoke the above shell script. BiaWeb2 will regenerate the entire tree, as it does not maintain 
the state of any incremental updates. If you are making major changes, it is recommended that 
you delete the output directory before re-generating, as this will be a "clean" build. Change
your `gensite.sh` script as below to ensure that the output directory is clean with no stale
content (i.e. removed files / folders from the source tree):

    !/bin/sh 
    rm -rf Out/
    /path/to/biaweb2/biaweb2 -i Home -t templates -o Out

### Recommendations for Uploading Content and Static Resources

Often, a website is more than just the HTML pages. You have images, for instance needed by
your pages. While BiaWeb2 does not handle static content, it's quite easy to deal with static
content, since they need to be uploaded to a fixed location on your website. The `gensite.sh`
script can be adapted to copy/sync your remote website with the generated output of BiaWeb2
as well as static resources by adding a couple of extra lines:

Assume that we keep our static resources (like images) in _images_ within the folder 
_/path/to/mywebsite/images_. We can set the script to automatically sync the images 
folder with your remote website (assumed to be accessible at `host@remote`). 


    !/bin/sh 
    rm -rf Out/
    /path/to/biaweb2/biaweb2 -i Home -t templates -o Out

    # Sync contents of the Out folder with the remote site at host@remote
    rsync -avrz --delete ~/path/to/mywebsite/Out/ host@remote:/path/to/www/

    # Sync static resources such as images to remote website
    rsync -avrz --delete /path/to/mywebsite/images/ host@remote:/path/to/www/images

The resources can then be linked from your individual pages with markdown like this

    ...
    # Heading 

    This is some content
    ![My image](images/testimage.jpg)

Since BiaWeb2 uses the `<base>` tag to define the base URL, the above code will work 
from any location on your website. 

## Further Customization and Notes

* You can edit the contents of the _templates_ folder to get a more unique look and feel. 
  Templates are just plain HTML bits with no logic (other than the template vars marked 
  within curly braces). The __style.css.tpl__ file is the CSS bit that is embedded in 
  all pages of the site. At present there is no way to get a different look and feel for
  different categories or parts of the site.

* The file __stringbits.txt__ contain general strings that are used to describe parts of
  your website. This can also be customized, but each line stands for a particular item 
  and should not be interchanged. Ensure that changes to a particular line describe the 
  same item (in a different way). For example the text `List of Articles` may be substituted
  by `List of Docs` or `Documents` but changing it to `Categories` will not make sense. 

* The generated website from the default template should conform to HTML 5. If there are
  any errors in the resultant HTML, please raise an 
  [issue](https://gitlab.com/harishankarv/biaweb2/-/issues/new) to 
  address it and it will be fixed. In general, please raise an issue for any bugs, 
  feedback or feature requests. However, as far as features are concerned, there is no 
  plan to make this a comprehensive website management tool. The key philosophy of this 
  tool is simplicity and I plan to keep it that way.

* __Limitation of RSS Feed:__ At present the RSS feed generated by the script in 
  __feed.xml__ does not fetch any articles from sub-categories and only fetches articles from'
  its own category. This may be addressed in the future, depending on whether this is seen
  as necessary.