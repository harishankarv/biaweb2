#ifndef __BIAWEBRSSFEED__
#define __BIAWEBRSSFEED__

#include "biawebutil.hpp"
#include "biawebtemplate.hpp"
#include <ctime>
#include <list>
#include "fmt/format.h"

namespace biaweb {
    // class to define and implement an RSS feed item
    class RSSFeedItem {
      protected:
        std::string title;
        std::string description;
        std::string url;
        std::time_t pub_date;
      public:
        // Main Constructor
        RSSFeedItem (std::string title, std::string desc, 
                        std::string url, std::time_t pub) {
            this->title = escape_html (title);
            this->description = escape_html (desc);
            this->url = url;
            this->pub_date = pub;
        }

        // generate HTML using specified template
        std::string to_html (Template *t);

        std::string get_title() {
        	return this->title;
        }
        void set_title(std::string title) {
        	this->title =  escape_html (title);
        }
        std::string get_description() {
        	return this->description;
        }
        void set_description(std::string description) {
        	this->description = escape_html (description);
        }
        std::string get_url() {
        	return this->url;
        }
        void set_url(std::string url) {
        	this->url = url;
        }
        std::time_t get_pub_date() {
        	return this->pub_date;
        }
        void set_pub_date(std::time_t pub_date) {
        	this->pub_date = pub_date;
        }
    };
    std::string RSSFeedItem::to_html (Template *t) {
        std::string item_tpl = t->get_rss_item_tpl ();

        std::tm ct = *std::localtime (&this->pub_date);

        std::string output = fmt::format (item_tpl, 
                                            fmt::arg ("title", this->title),
                                            fmt::arg ("description", this->description),
                                            fmt::arg ("url", this->url),
                                            fmt::arg ("pubdate", ct));
        return output;
    }

    // class to implement an RSS feed
    class RSSFeed {
      protected:
        std::string title; 
        std::time_t pub_date;
        std::list<RSSFeedItem> items;
      public:
        // add a feed item 
        void add_rss_item (RSSFeedItem item) {
            this->items.insert (items.cend(), item);
        }

        // number of items
        std::size_t get_num_items () {
            return this->items.size ();
        }

        std::string get_title () {
            return this->title;
        }
        void set_title (std::string title) {
            this->title = escape_html (title);
        } 

        std::time_t get_pub_date() {
        	return this->pub_date;
        }
        void set_pub_date(std::time_t pub_date) {
        	this->pub_date = pub_date;
        }

        // output the RSS feed file as feed.xml
        void output_to_html (Template *t, std::string path) {
            std::string templ_str = t->get_rss_tpl ();
            // only if there are feed items, output the feed.xml file
            if (this->items.size () != 0) {
                std::string items_str ;
                // get the items 
                for (auto item : this->items) 
                    items_str += item.to_html (t);
                
                std::tm ct = *std::localtime (&this->pub_date);

                std::string output = fmt::format (templ_str, 
                                            fmt::arg ("pubdate", ct),
                                            fmt::arg ("items", items_str),
                                            fmt::arg ("title", this->title));
                std::ofstream f (path + "/feed.xml");
                f << output;
                f.close ();
            }
        }
    };
}

#endif 