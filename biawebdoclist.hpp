#ifndef __BIAWEBDOCITEMLIST__
#define __BIAWEBDOCITEMLIST__
#include <ctime>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <list>
#include <fmt/format.h>
#include <fmt/chrono.h>
#include "biawebstrings.hpp"
#include "biawebdocument.hpp"
#include "biawebtemplate.hpp"

// to implement a list of items (documents) with creation/modified date/time display
namespace biaweb {
    // to implement a single document item
    class DocListItem {
      protected:
      // Just the required fields to build the item
        std::string title; 
        std::string url;
        std::string desc;
        std::time_t ctime;
        std::time_t mtime;
      public:
        DocListItem (Document *doc, std::string urlpath) {
            this->title = doc->get_title ();
            this->url = urlpath + doc->get_filename() + ".html";
            this->ctime = doc->get_creation_date (); 
            this->mtime = doc->get_modified_date ();
            this->desc = doc->get_meta_desc ();
        }

        std::string get_desc () {
            return this->desc;
        }

        std::time_t get_mtime() {
            return this->mtime;
        }

        std::time_t get_ctime() {
            return this->ctime;
        }

        std::string get_url() {
	          return this->url;
        }

        std::string get_title() {
            return this->title;
        }
        
        // output to HTML vide the template
        std::string to_html (Template *t);
    };
    
    // output to HTML vide the template 
    std::string DocListItem::to_html (Template *t) {
        std::string templstr = t->get_doclistitem_tpl ();
        std::tm c, m;
        c = *std::localtime (&this->ctime);
        m = *std::localtime (&this->mtime);
        
        std::string outputhtml = fmt::format (templstr, fmt::arg("url", this->url),
                               fmt::arg("doctitle", this->title),
                               fmt::arg("desc", this->desc), 
                               fmt::arg("cdate", c),
                               fmt::arg("mdate", m));
        
        return outputhtml;
    }

    // to implement a document list (or table)
    class DocList {
      protected:
        std::string title;
        std::list<DocListItem> items;
      public:
        void set_title (std::string title) {
            this->title = title;
        }
        // add a document item
        void add_document_item (DocListItem docitem) {
            this->items.insert (this->items.cend(), docitem);
        }
        // render to HTML from a template
        std::string to_html (Template *t);
    };

    std::string DocList::to_html (Template *t) {
        std::string templstr = t->get_doclist_tpl ();

        std::string outputhtml = "";
        // if the number of elements is non zero
        if (this->items.size () != 0) {
            std::string docitems = "";
            for (DocListItem item : this->items)
                docitems += item.to_html (t);

            outputhtml = fmt::format (templstr, 
                                            fmt::arg ("title", this->title),
                                            fmt::arg ("docitems", docitems));
        }
        return outputhtml;
    }
}

#endif