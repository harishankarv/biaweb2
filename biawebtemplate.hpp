#ifndef __BIAWEBTEMPLATE__
#define __BIAWEBTEMPLATE__

#include <array>
#include <fstream>
#include "biawebutil.hpp"

namespace biaweb {
    // class to load template from a directory for use with classes requiring to output 
    // via templates
    class Template {
      protected:
        // all the template bits stored as strings
        std::string main_tpl;
        std::string doclist_tpl;
        std::string doclistitem_tpl;
        std::string genericlinklistitem_tpl;
        std::string genericlistitem_tpl;
        std::string navigationbit_tpl;
        std::string sidebar_tpl;
        std::string style_tpl;
        std::string rss_tpl;
        std::string rss_item_tpl;
        std::array<std::string, 8> stringbits;
      public:
        // constructor: generate the template bits from a template directory 
        Template (std::string templatedir) ;

        // getters;
        std::string get_main_tpl () { return this->main_tpl; }
        std::string get_doclist_tpl () { return this->doclist_tpl; }
        std::string get_doclistitem_tpl () { return this->doclistitem_tpl; }
        std::string get_genericlistitem_tpl () { return this->genericlistitem_tpl; }
        std::string get_genericlinklistitem_tpl () { return this->genericlinklistitem_tpl; }
        std::string get_navigationbit_tpl () { return this->navigationbit_tpl; }
        std::string get_sidebar_tpl () { return this->sidebar_tpl; }
        std::string get_style_tpl () { return this->style_tpl; } 
        std::string get_stringbit (unsigned int id) { return this->stringbits[id]; }
        std::string get_rss_tpl () { return this->rss_tpl; }
        std::string get_rss_item_tpl () { return this->rss_item_tpl; }
    };

    // construct a template from a source directory
    Template::Template (std::string templatedir) {
        // read the templates
        this->main_tpl = load_from_file (templatedir + "/main.tpl.html");
        this->doclist_tpl = load_from_file (templatedir + "/doclist.tpl.html");
        this->doclistitem_tpl = load_from_file (templatedir + "/doclistitem.tpl.html");
        this->genericlinklistitem_tpl = load_from_file 
                        (templatedir + "/genericlinklistitem.tpl.html");
        this->genericlistitem_tpl = load_from_file (templatedir + "/genericlistitem.tpl.html");
        this->navigationbit_tpl = load_from_file (templatedir + "/navigationbit.tpl.html");
        this->sidebar_tpl = load_from_file (templatedir + "/sidebar.tpl.html");
        this->style_tpl = load_from_file (templatedir + "/style.tpl.css");
        this->rss_tpl = load_from_file (templatedir + "/rss.tpl.xml");
        this->rss_item_tpl = load_from_file (templatedir + "/rssitem.tpl.xml");

        // read the stringbits into the array
        std::ifstream f (templatedir + "/stringbits.txt");
        unsigned int i = 0;
        std::string line;
        while (! f.eof () && i < this->stringbits.size ()) {
            std::getline (f, line);
            this->stringbits[i] = line;
            i ++;
        }
        f.close ();
    }

}

#endif