#include "biawebmain.hpp"
#include <unistd.h>
using namespace biaweb; 
int main (int argc, char **argv) {
    Main prog (argc, argv);
    return prog.run ();
}